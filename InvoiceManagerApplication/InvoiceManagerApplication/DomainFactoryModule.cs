﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ninject;
using Ninject.Modules;
using InvoiceManagerApplication.Models;
using InvoiceManagerApplication.Persistence;

namespace InvoiceManagerApplication.App_Start
{
    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IInvoiceInbox>().To<InvoiceInbox>().InSingletonScope();
            Bind<IUserManager>().To<UserManager>().InSingletonScope();
            Bind<IInvoiceRepository>().To<InvoiceRepository>().InSingletonScope();
        }
    }
}