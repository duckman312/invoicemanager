﻿using InvoiceManagerApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceManagerApplication.Persistence
{
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> InvoiceRepoList { get; }

        IEnumerable<User> UserRepoList { get; }

        void SaveInvoice(Invoice invoice);
    }
}
