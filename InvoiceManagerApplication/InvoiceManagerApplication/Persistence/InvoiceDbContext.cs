﻿using InvoiceManagerApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InvoiceManagerApplication.Persistence
{
    public class InvoiceDbContext : DbContext
    {
        /// <summary>
        /// Creates the DB Context to the connection 'InvoiceDbConnection'
        /// </summary>
        public InvoiceDbContext() : base("InvoiceDbConnection")
        {

        }
        /// <summary>
        /// Invoice Table
        /// </summary>
        public DbSet<Invoice> Invoices { get; set; }
        /// <summary>
        /// User Table
        /// </summary>
        public DbSet<User> Users { get; set; }
    }
}