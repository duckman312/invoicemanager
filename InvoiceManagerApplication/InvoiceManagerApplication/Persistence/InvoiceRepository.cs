﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InvoiceManagerApplication.Models;

namespace InvoiceManagerApplication.Persistence
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private InvoiceDbContext _dbContext;

        public InvoiceRepository()
        {
            _dbContext = new InvoiceDbContext();
        }
        public IEnumerable<Invoice> InvoiceRepoList
        {
            get
            {
                return _dbContext.Invoices;
            }
        }
        public IEnumerable<User> UserRepoList
        {
            get
            {
                return _dbContext.Users;
            }
        }
        public void SaveInvoice(Invoice invoice)
        {
            if (invoice.InvoiceID == -1)
            {
                _dbContext.Invoices.Add(invoice);
            }else
            {
                Invoice invoiceEntity = _dbContext.Invoices.Find(invoice.InvoiceID);

                invoiceEntity.Change(invoice);
            }

            _dbContext.SaveChanges();
        }

    }
}