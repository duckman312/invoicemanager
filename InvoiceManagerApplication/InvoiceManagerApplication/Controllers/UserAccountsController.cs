﻿using InvoiceManagerApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceManagerApplication.Controllers
{
    public class UserAccountsController : Controller
    {

        private IUserManager _userManager;

        public UserAccountsController(IUserManager userManager)
        {
            _userManager = userManager;
        }
        /// <summary>
        /// Inital Action. Shows the Main Login Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View("Index");
        }
        /// <summary>
        /// Process the users login and will return to the login if the user fails
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="submit"></param>
        /// <returns></returns>
        public ActionResult LoginAttempt(string UserName, string Password, string submit)
        {
            //tracks if login was a success
            bool success = true;
            bool manager = false;
            // used to process login
            switch (submit)
            {
                //checks if they are a manager
                case "Manager Login":
                    if (!_userManager.ValidateManager(UserName,Password))
                    {
                        ViewBag.IsManager = false;
                        success = false;
                    }else
                    {
                        manager = true;
                    }
                    //no fall through in C# :(
                    goto case "User Login";
                //checks if the user entered a valid password
                case "User Login":
                    
                    if (!_userManager.Validate(UserName,Password))
                    {
                        ViewBag.Valid = false;
                        success = false;
                    }
                    break;          
            }
            //return them to login if they failed
            if (!success)
            {
                return View("Index");
            }else
            {
                //save their info to the session and proceed
                Session["UserName"] = UserName;
                Session["Manager"] = manager; 
                return View("AccountMain");
            }
        }
        public ActionResult AccountMain()
        {
            return View("AccountMain");
        }
    }
}