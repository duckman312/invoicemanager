﻿using InvoiceManagerApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceManagerApplication.Controllers
{
    public class InvoiceController : Controller
    {
        private IInvoiceInbox _inbox;
        public InvoiceController(IInvoiceInbox inbox)
        {
            _inbox = inbox;
        }
        /// <summary>
        /// used to add a new invoice
        /// </summary>
        /// <returns></returns>
        public ActionResult AddInvoice()
        {
            return View("AddEditInvoices");
        }
        /// <summary>
        /// Used to edit an existing invoice
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public ActionResult EditInvoice(int invoiceID)
        {
            Invoice inv = _inbox.GetInvoice(invoiceID);
            return View("AddEditInvoices",inv);
        }
        /// <summary>
        /// Shows the review Page
        /// </summary>
        /// <returns></returns>
        public ActionResult ReviewInvoices()
        {
            HttpContext.Cache["INBOX"] = _inbox;
            IEnumerable<Invoice> unpaiedInvoices = _inbox.GetUnPaidInvoices();
            ViewBag.Total = _inbox.GetUnpaidTotal();
            return View("InvoiceReview",unpaiedInvoices);
        }
        /// <summary>
        /// used to process the Invoice forum input
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public ActionResult ProcessInvoice(Invoice invoice, string submit)
        {
            switch (submit)
            {
                //for submiting new invoices
                case "Submit Invoice":
                    _inbox.AddInvoice(invoice);
                    HttpContext.Cache["INBOX"] = _inbox;
                    return RedirectToAction("AccountMain", "UserAccounts");
                //for updating existing invoices
                case "Save Invoice":
                    _inbox.AddInvoice(invoice);
                    HttpContext.Cache["INBOX"] = _inbox;
                    return RedirectToAction("ReviewInvoices");
            }
            return null;
        }

    }
}