﻿using InvoiceManagerApplication.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceManagerApplication.Models
{
    public class InvoiceInbox : IInvoiceInbox
    {

        private IInvoiceRepository _repository;
        /// <summary>
        /// Property to iterate through the invoices
        /// </summary>
        public IEnumerable<Invoice> InvoiceList { get; }
        /// <summary>
        /// Constructor for the InvoiceInbox
        /// </summary>
        public InvoiceInbox(IInvoiceRepository repo)
        {
            _repository = repo;
            InvoiceList = repo.InvoiceRepoList;
        }
        /// <summary>
        /// Adds an invoice to the inbox
        /// </summary>
        /// <param name="invoice"></param>
        public void AddInvoice(Invoice invoice)
        {
            _repository.SaveInvoice(invoice);
        }
        /// <summary>
        /// Gets all of the unpaid invoices in the inbox
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Invoice> GetUnPaidInvoices()
        {
            foreach (Invoice inv in InvoiceList)
            {
                if (!inv.Paid)
                {
                    yield return inv;
                }
            }
        }
        /// <summary>
        /// Gets a single invoice based on the InvoiceID
        /// </summary>
        /// <param name="InvoiceID"></param>
        /// <returns></returns>
        public Invoice GetInvoice(int invoiceID)
        {
            foreach(Invoice inv in InvoiceList)
            {
                if (inv.InvoiceID == invoiceID)
                {
                    return inv;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets the total that is owed across all of the unpaid invoices
        /// </summary>
        /// <returns></returns>
        public decimal GetUnpaidTotal()
        {
            decimal total = 0;
            foreach(Invoice inv in this.GetUnPaidInvoices())
            {
                total += inv.GetTotal();
            }
            return total;
        }
    }
}