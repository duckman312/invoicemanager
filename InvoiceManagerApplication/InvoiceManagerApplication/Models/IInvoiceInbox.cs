﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceManagerApplication.Models
{
    public interface IInvoiceInbox
    {
        IEnumerable<Invoice> InvoiceList { get; }

        void AddInvoice(Invoice invoice);

        Invoice GetInvoice(int invoiceID);

        Decimal GetUnpaidTotal();

        IEnumerable<Invoice> GetUnPaidInvoices();
    }
}
