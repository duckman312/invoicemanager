﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceManagerApplication.Models
{
    public class Invoice
    {
        //some constants to help track data and calculate tax
        public static decimal TAX = .10M; 
        public static int COUNT = 0;
        public int InvoiceID { get; set; } = -1;
        public string ClientName { get; set; }
        public string ClientAddress { get; set; }
        public DateTime ShipmentDate { get; set; }
        public string ProductName { get; set; }
        public DateTime PaymentDue { get; set; }
        public int ProductQuantity { get; set; }
        public decimal UnitPrice { get; set; }
        public Currency CurrencyType { get; set; }
        public bool Paid { get; set; } = false; 
        /// <summary>
        /// gets total with taxes
        /// </summary>
        /// <returns></returns>
        public decimal GetTotal()
        {
            return GetSubTotal() + GetTaxes();
        }
        /// <summary>
        /// Used to change all of the values of an invoice
        /// </summary>
        /// <param name="invoice"></param>
        internal void Change(Invoice invoice)
        {
            this.InvoiceID = invoice.InvoiceID;
            this.ClientName = invoice.ClientName;
            this.ClientAddress = invoice.ClientAddress;
            this.ShipmentDate = invoice.ShipmentDate;
            this.ProductName = invoice.ProductName;
            this.PaymentDue = invoice.PaymentDue;
            this.UnitPrice = invoice.UnitPrice;
            this.CurrencyType = invoice.CurrencyType;
            this.Paid = invoice.Paid;
        }

        /// <summary>
        /// gets the total without taxes
        /// </summary>
        /// <returns></returns>
        public decimal GetSubTotal()
        {
            return UnitPrice * ProductQuantity;
        }

        /// <summary>
        /// gets just the taxes
        /// </summary>
        /// <returns></returns>
        public decimal GetTaxes()
        {
            return GetSubTotal() * TAX;
        }
    }
    public enum Currency : byte
    {
        CAD,
        US,
        EUR
    }
}