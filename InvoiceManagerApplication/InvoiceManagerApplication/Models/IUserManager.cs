﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceManagerApplication.Models
{
    public interface IUserManager
    {
        /// <summary>
        /// Used to validate a reguler user logging in
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool Validate(string username, string password);

        /// <summary>
        /// Used to validate a manager user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool ValidateManager(string username, string password);
    }
}
