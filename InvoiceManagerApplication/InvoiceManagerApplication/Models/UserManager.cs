﻿using InvoiceManagerApplication.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InvoiceManagerApplication.Models
{
    public class UserManager : IUserManager
    {
        private IInvoiceRepository _repo;

        public UserManager(IInvoiceRepository repo)
        {
            _repo = repo;
        }
        
        public bool Validate(string username, string password)
        {
            IEnumerable<User> matches = _repo.UserRepoList.Where(user =>
                user.UserName.Equals(username) && user.Password.Equals(password));

            return matches.Any();
        }

        public bool ValidateManager(string username, string password)
        {
            IEnumerable<User> matches = _repo.UserRepoList.Where(user =>
                user.UserName.ToLower().Equals(username.ToLower()) && user.Password.ToLower().Equals(password.ToLower())
                && user.Type == UserType.Manager);

            return matches.Any();
        }
    }
}