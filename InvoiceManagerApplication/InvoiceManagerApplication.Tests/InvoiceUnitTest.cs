﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using InvoiceManagerApplication.Models;

namespace InvoiceManagerApplication.Tests
{
    [TestClass]
    public class InvoiceUnitTest
    {
        [TestMethod]
        public void InvoiceSubTotalTest()
        {
            Invoice inv = new Invoice() {
                ProductQuantity=10,
                UnitPrice=10
            };
            //subtotal = 100, 10*10
            Assert.IsTrue(inv.GetSubTotal() == 100);
        }
        [TestMethod]
        public void InvoiceTaxesTest()
        {
            Invoice inv = new Invoice()
            {
                ProductQuantity = 10,
                UnitPrice = 10
            };
            //taxes = subtotal*TAX, TAX = 0.10
            Assert.IsTrue(inv.GetTaxes() == 10);
        }

        [TestMethod]
        public void InvoiceTotalTest()
        {
            Invoice inv = new Invoice()
            {
                ProductQuantity = 10,
                UnitPrice = 10
            };
            //Total = 110, subtotal + taxes (100+10)
            Assert.IsTrue(inv.GetTotal() == 110);
        }

    }
}
