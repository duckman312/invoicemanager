﻿using InvoiceManagerApplication.App_Start;
using InvoiceManagerApplication.Models;
using InvoiceManagerApplication.Persistence;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceManagerApplication.Tests
{
    [TestClass]
    public class ReceivablesUnitTest
    {
        private IKernel _diKernel = new StandardKernel();

        private InvoiceInbox BuildInbox()
        {
            Mock<IInvoiceRepository> testInvoiceRepo = new Mock<IInvoiceRepository>();

            testInvoiceRepo.Setup(m => m.InvoiceRepoList).Returns(
                new List<Invoice>()
                {
                    new Invoice()
                    {
                        InvoiceID=0,
                        UnitPrice=10,
                        ProductQuantity=10
                    },
                    new Invoice()
                    {
                        InvoiceID=1,
                        UnitPrice=10,
                        ProductQuantity=10
                    },
                    new Invoice()
                    {
                        InvoiceID=2,
                        UnitPrice=10,
                        ProductQuantity=10
                    }
                }    
            );

            _diKernel.Load(new DomainFactoryModule());

            _diKernel.Rebind<IInvoiceRepository>().ToConstant(testInvoiceRepo.Object);

            InvoiceInbox inbox = new InvoiceInbox(_diKernel.Get<IInvoiceRepository>());
            return inbox;
        }

        [TestMethod]
        public void InboxTotalTest()
        {
            InvoiceInbox inbox = BuildInbox();
            //total = total1+total2+total3
            //total = 120+110+110=330
            Assert.IsTrue(inbox.GetUnpaidTotal() == 330);
        }

        [TestMethod]
        public void InboxChangeTest()
        {
            InvoiceInbox inbox = BuildInbox();
            //replace id 0 worth 100
            Invoice inv = new Invoice() {
                InvoiceID = 0,
                ProductQuantity = 10,
                UnitPrice = 10,
                Paid = true
            };
            inbox.AddInvoice(inv);
            //new unpaid value should 220
            //total 2 + total 3
            //110+110
            Assert.IsTrue(inbox.GetUnpaidTotal()==220);
        }
    }
}
